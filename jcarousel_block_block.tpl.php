<?php
  drupal_add_js(drupal_get_path('module', 'jcarousel_block') . '/jcarousel/lib/jquery.jcarousel.pack.js');
  drupal_add_css(drupal_get_path('module', 'jcarousel_block') . '/jcarousel/lib/jquery.jcarousel.css');
  drupal_add_css(drupal_get_path('module', 'jcarousel_block') . '/jcarousel/skins/' . $skin . '/skin.css');

  // load the node if it's in a node page
  if ( arg(0) == 'node' && is_numeric(arg(1)) && ! arg(2) ) {
        $node = node_load(arg(1));
  } 
?>
<div id='wrap'>
    <script type="text/javascript">
      $(document).ready(function() {
       	$('#mycarousel').jcarousel();
		  });
    </script>
				
	<div class="carousel_container">
<?php
  if($imagefield && $node->$imagefield){
?>
    <ul class="jcarousel-skin-<?php print $skin;?>" id="mycarousel">
<?php
    foreach($node->$imagefield as $key=>$image){
?>
      <li><a href="<?php print $image[$imagefield_path] ?>" rel="<?php print $image_rel; ?>"><img width="<?php print $image[$imagefield_width];?>" height="<?php print $image[$imagefield_height];?>" alt="<?php print $image[$imagefield_alt];?>" src="<?php print $image[$imagefield_path];?>" /></a></li>
<?php
    } // foreach
?>
    </ul>
<?php
  }else{
    print(t("Imagefield is not set. Please do the setup at admin/settings/jcarousel_block"));
  }
?>
	</div> <!-- carousel_container -->
</div> <!-- wrap -->

